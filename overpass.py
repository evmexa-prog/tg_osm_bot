import requests


class Overpass:
    def __init__(self):
        self.overpass_url = "https://overpass-api.de/api/interpreter"
        self.getparams = {'data': ''}

    def get_custom_poi_around_coords(self, tag, radius, lat, lon):
        self.getparams['data'] = f"""
                [out:json];
                node[{tag}](around:{radius}, {lat}, {lon});
                out geom;
                """
        response = requests.get(url=self.overpass_url, params=self.getparams)
        if response.status_code == 200:
            data = response.json()
            return data
        else:
            print('Ошибка', response.status_code)

    def get_shops_around_coords(self, radius, lat, lon):
        self.getparams['data'] = f"""
        [out:json];
        node["shop"](around:{radius}, {lat}, {lon});
        out geom;
        """
        response = requests.get(url=self.overpass_url, params=self.getparams)
        if response.status_code == 200:
            data = response.json()
            return data
        else:
            print('Ошибка', response.status_code)
