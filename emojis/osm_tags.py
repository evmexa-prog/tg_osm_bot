
# emoji from https://carpedm20.github.io/emoji/all.html
# single tags - tags like tag = completed value
single_tags_keys = {
    'name': ['Название', '\U0001f58b\ufe0f'],
    'name:en': ['Название на англ.'],
    'alt_name': ['Другое название'],

    'contact:email': ['Е-мэил', '\U0001f4e7'],
    'contact:ok': ['Одноклассники'],
    'contact:vk': ['ВКонтакте'],
    'contact:facebook': ['Фейсбук'],
    'contact:website': ['Сайт', '\U0001f310'],
    'contact:phone': ['Телефон', '\U0001f4de'],
    'contact:telegram': ['Телеграм'],

    'brand:wikipedia': ['Википедия', '\U0001f4d7'],
    'wikipedia': ['Википедия', '\U0001f4d7'],

    'opening_hours': ['Часы работы', '\U0001f554'],

    'addr:floor': ['Этаж', '\U0001fa9c'],
    'level': ['Этаж(уровень)', '\U0001fa9c'],
}
# values for tags that need to be translated or clarified
tags_values = {
    # key = shop
    # values:
    'bakery': ['Булочная'],
    'butcher': ['Мясной магазин'],
    'cheese': ['Сырный магазин'],
    'chocolate': ['Магазин шоколада'],
    'coffee': ['Кофейная'],
    'confectionery': ['Кондитерская'],
    'convenience': ['Магазин у дома'],
    'dairy': ['Магазин молочных продуктов'],
    'greengrocer': ['Фрукты и овощи'],
    'pasta': ['Магазин пасты'],
    'seafood': ['Магазин морепродуктов'],
    'spices': ['Магазин специй'],
    'tea': ['Магазин чая'],
    'water': ['Магазин воды'],
    'department_store': ['Универмаг'],
    'mall': ['Молл'],
    'supermarket': ['Супермаркет'],
    'baby_goods': ['Магазин детских товаров'],
    'bag': ['Магазин сумок'],
    'boutique': ['Бутик'],
    'clothes': ['Магазин одежды'],
    'fabric': ['Магазин тканей'],
    'jewelry': ['Ювелирный'],
    'shoes': ['Обувной магазин'],
    'watches': ['Магазин часов'],
    'charity': ['Благотворительный магазин'],
    'second_hand': ['Секонд-хенд'],
    'variety_store': ['Розничный магазин'],
    'hairdresser': ['Парикмахерская'],
    'optician': ['Оптика'],
    'fireplace': ['Печной магазин'],
    'doityourself': ['Строительный гипермаркет'],
    'trade': ['Оптовый склад'],
    'car': ['Автосалон'],
    'computer': ['Компьютерный магазин'],
    'furniture': ['Мебельный магазин'],
    'fishing': ['Рыболовный магазин'],
    'ski': ['Магазин лыж'],
    'sports': ['Спортивный магазин'],
    'tyres': ['Магазин шин'],
    'toys': ['Магазин игрушек'],
}