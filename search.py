import requests

class Search:
    def __init__(self, output_format="json"):
        self.nominatim_url = "https://nominatim.openstreetmap.org/search"
        self.output_format = output_format
        self.getparams = {'q': '',
                          'format': str(output_format),
                          'addressdetails': 1
                          }

    def get_places(self):
        response = requests.get(url=self.nominatim_url, params=self.getparams)
        if response.status_code == 200:
            data = response.json()
            # print(data)
            # if len(data) >= 1:
            #     print(data[0]['lat'], data[0]['lon'])
            return data
            # else:
            #     print('Нету такого')
        else:
            print('Ошибка')

def main():
    s = Search()
    s.get_places()
if __name__ == "__main__":
    main()