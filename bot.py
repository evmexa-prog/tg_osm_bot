from __future__ import unicode_literals
import emojis.country
import telebot
from telebot import types
from search import Search
from overpass import Overpass
import config
import emojis.osm_tags as tags

s = Search()
o = Overpass()
bot = telebot.TeleBot(config.TOKEN)


def overpass_print_res(res, mes):
    answ = ""
    if len(res['elements']) != 0:
        for i in range(len(res['elements'])):
            answ += f"POI <b>{i + 1}</b>/<b>{len(res['elements'])}</b> \n"
            answ += f"Координаты: <a href=\"https://www.openstreetmap.org/#map=19/{res['elements'][i]['lat']}/" \
                    f"{res['elements'][i]['lon']}\">{res['elements'][i]['lat']}, {res['elements'][i]['lon']}</a>\n\n "
            for tag in res['elements'][i]['tags']:
                tag_value = res['elements'][i]['tags'][tag]
                if tag in tags.single_tags_keys:
                    text_value = tags.single_tags_keys[tag][0]
                    if len(tags.single_tags_keys[tag]) != 1:
                        emoji = tags.single_tags_keys[tag][1]
                        if tag == 'brand:wikipedia':
                            answ += f"{emoji} {text_value} <b><a " \
                                    f"href=\"https://wikipedia.org/wiki/{tag_value}\">" \
                                    f"{tag_value}</a></b>\n"
                        else:
                            answ += f"{emoji} {text_value} <b>{tag_value}</b>\n"
                    else:
                        answ += f"{text_value} <b>{tag_value}</b>\n"

            bot.send_message(mes.chat.id, answ, disable_web_page_preview=True, parse_mode="HTML")
            answ = ""
    else:
        bot.send_message(mes.chat.id, "Результатов не найдено")
    bot.send_message(mes.chat.id, "Данные - участники OpenStreetMap, ODbL 1.0")


def nominatim_print_res(query, res, mes, find_all=False):
    flag = ""
    if find_all:
        bot.send_message(mes.chat.id, f"Места по запросу *{query}*:", parse_mode="Markdown")
        bot.send_message(mes.chat.id, f"Получен(о) *{len(res)}* результат(ов/а)", parse_mode="Markdown")
        for i in range(len(res)):
            country_code = (res[i]['address']['country_code']).upper()
            if country_code in emojis.country.flags:
                flag = emojis.country.flags[country_code]
            output_mes = f"Результат *{i + 1}*/*{len(res)}*\n" \
                         f"{flag} _{res[i]['display_name']}_\n" \
                         "\n" \
                         f"*Координаты*: [{res[i]['lat']}, {res[i]['lon']}](https://www.openstreetmap.org/#map=17/" \
                         f"{res[i]['lat']}/{res[i]['lon']}) \n" \
                         "Данные - участники OpenStreetMap, ODbL 1.0"
            bot.send_message(mes.chat.id, output_mes, parse_mode="Markdown", disable_web_page_preview=True)
    else:
        country_code = (res[0]['address']['country_code']).upper()
        if country_code in emojis.country.flags:
            flag = emojis.country.flags[country_code]
        output_mes = f"Место по запросу *{query}*: \n\n" \
                     f"{flag} _{res[0]['display_name']}_\n" \
                     "\n" \
                     f"Координаты: [{res[0]['lat']}, {res[0]['lon']}](https://www.openstreetmap.org/#map=17/" \
                     f"{res[0]['lat']}/{res[0]['lon']}) \n" \
                     "Данные - участники OpenStreetMap, ODbL 1.0"
        bot.send_message(mes.chat.id, output_mes, parse_mode="Markdown", disable_web_page_preview=True)


@bot.message_handler(commands=["find"])
def nominatim_find(mes):
    query = mes.text.split(maxsplit=1)[1]
    s.getparams['q'] = query
    res = s.get_places()
    if len(res) != 0:
        nominatim_print_res(query, res, mes)
    else:
        bot.send_message(mes.chat.id, "Место не найдено")


@bot.message_handler(commands=["find_all"])
def nominatim_find_all(mes):
    query = mes.text.split(maxsplit=1)[1]
    s.getparams['q'] = query
    res = s.get_places()
    if len(res) != 0:
        nominatim_print_res(query, res, mes, find_all=True)
    else:
        bot.send_message(mes.chat.id, "Место не найдено")


@bot.message_handler(commands=["get_custom_poi_around_coords"])
def custom_poi_around_coords(mes):
    query = mes.text.split(maxsplit=5)
    if len(query) != 5 or "." in query or "," in query:  # 5 - включая команду
        bot.send_message(mes.chat.id, "Введите *4* аргументов к команде:", parse_mode="Markdown")
        bot.send_message(mes.chat.id, "1 - тег в формате *ключ=значение* или *ключ* \n2 - радиус поиска(в метрах) "
                                      "\n3 - широта \n4 - долгота", parse_mode="Markdown")
        bot.send_message(mes.chat.id, "При вводе координат между ними не должно быть запятых, точек и иных символов")
        # parse_mode - HTML т.к в shops_around_coords встречается "_"
        bot.send_message(mes.chat.id, "<i>P.S</i> Команда ищет все POI с вашими тегами. Если вы хотите найти только "
                                      "магазины(ключ shop), то обращайтесь к <i>/shops_around_coords</i>",
                         parse_mode="HTML")
    else:
        if query[1].isdigit():
            bot.send_message(mes.chat.id, "Первый параметр должен быть тегом, который вы хотите найти в формате "
                                          "*ключ=значение* или *ключ*", parse_mode="Markdown")
        else:
            try:
                res = o.get_custom_poi_around_coords(query[1], float(query[2]), float(query[3]), float(query[4]))
                overpass_print_res(res, mes)
            except TypeError as te:
                bot.send_message(mes.chat.id, "Первый параметр должен быть тегом, который вы хотите найти в формате "
                                              "*ключ=значение* или *ключ*", parse_mode="Markdown")
            except ValueError as ve:
                bot.send_message(mes.chat.id, "При вводе координат между ними не должно быть запятых или точек,"
                                              " и иных символов")


@bot.message_handler(commands=["shops_around_coords"])
def shops_around_coords(mes):
    query = mes.text.split(maxsplit=4)
    if len(query) != 4 or "." in query or "," in query:  # 4 - включая команду
        bot.send_message(mes.chat.id, "Введите *3* аргументов к команде:", parse_mode="Markdown")
        bot.send_message(mes.chat.id, "1 - радиус поиска(в метрах) \n2 - широта \n3 - долгота")
        bot.send_message(mes.chat.id, "При вводе координат между ними не должно быть запятых, точек и иных символов")
        # parse_mode - HTML т.к в shops_around_coords встречается "_"
        bot.send_message(mes.chat.id, "<i>P.S</i> Команда ищет только POI с ключом shop(магазин). Если вы хотите "
                                      "найти POI с другими ключами, то обращайтесь к "
                                      "<i>/get_custom_poi_around_coords</i>", parse_mode="HTML")
    else:
        try:
            res = o.get_shops_around_coords(float(query[1]), float(query[2]), float(query[3]))
            overpass_print_res(res, mes)
        except ValueError as e:
            bot.send_message(mes.chat.id, "При вводе координат между ними не должно быть запятых или точек,"
                                          " и иных символов")


@bot.message_handler(commands=["start"])
def start(mes):
    bot.send_message(mes.chat.id, "Привет, я бот для OpenStreetMap(OSM)")
    bot.send_message(mes.chat.id, "Используй команду /find или /find_all для поиска места с помощью Nominatim")
    bot.send_message(mes.chat.id, "Используй команду /shops_around_coords для поиска POI только с ключом shop("
                                  "магазины) в вашем радиусе n с помощью Overpass API")
    bot.send_message(mes.chat.id, "Используй команду /get_custom_poi_around_coords для поиска POI с вашими ключами "
                                  "радиусе n с помощью Overpass API")


@bot.message_handler(content_types=['text'])
def text(mes):
    bot.send_message(mes.chat.id, "Используй команду /find или /find_all для поиска места с помощью Nominatim")
    bot.send_message(mes.chat.id, "Используй команду /shops_around_coords для поиска POI только с ключом shop("
                                  "магазины) в вашем радиусе n с помощью Overpass API")
    bot.send_message(mes.chat.id, "Используй команду /get_custom_poi_around_coords для поиска POI с вашими ключами "
                                  "радиусе n с помощью Overpass API")


bot.polling(none_stop=True)
